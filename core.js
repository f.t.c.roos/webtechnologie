console.log("core.js is loaded");

//Generates the content of the selector and options
function selectorArea(optionTitles){
    var select = document.getElementById("selectElement");
       
    function selectorText(pString){
    var option = document.createElement("option");
    var optionText = document.createTextNode(pString);
    option.appendChild(optionText);
    select.appendChild(option);
    }

    //Assign an ID to each option
    for(var i = 0; i < allElements.length; i++){
        allElements[i].setAttribute("id", i)
    }    
            
    for (var item of optionTitles) {
        selectorText(item.localName);
    }
}
//Generates a node list of all the editable objects
allElements = document.querySelectorAll("body, header, nav, article, footer, section, aside, table");
selectorArea(allElements);

//Gives button a text and an event
document.getElementById("applyBtn").textContent = "Apply"; 
document.getElementById("applyBtn").addEventListener("click", applyStyle);

//Function for adjusting elements when clicking the Apply button
function applyStyle(){
    valueSE = document.getElementById("selectElement").selectedIndex;
        
    valueSC = document.getElementById("selectColor").value;
    if(valueSC != "Select Color"){
        document.getElementById(valueSE).style.backgroundColor = valueSC;
    }

    valueSF = document.getElementById("selectFontSize").value;

    if(valueSF != "Select Fontsize"){
        var textTags = document.getElementById(valueSE).querySelectorAll("h1, h2, h3, h6, p, a, li, select, option, button, th, td");
        for (var textTag of textTags) {
            if (valueSF == "Small")
                textTag.setAttribute("id", "smallText");
            else if (valueSF == "Normal")
                textTag.setAttribute("id", "normalText");
            else if (valueSF = "Big")
                textTag.setAttribute("id", "bigText");
        }
    }
}

//Generates the text of the footer-header
var selectorHeader = document.getElementById("selectorHeader");
var selectorHeaderText = document.createTextNode("Visual Preferences: ");
selectorHeader.appendChild(selectorHeaderText);