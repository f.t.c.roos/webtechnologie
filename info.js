console.log("info.js is loaded")
//Declaration of class Book
class Book {
    constructor(title, author, genre, publisher, pages, ISBN, edition, language) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.publisher = publisher;
        this.pages = pages;
        this.ISBN = ISBN;
        this.edition = edition;
        this.language = language;
    }
}
//Creating the instance of our book
const bookOrigin = new Book("Origin", "Dan Brown", "Thriller/Crime", "Doubleday",
                            "461", "978-0-593-07875-4", "1st", "English");


//Declaration of the extension of class Book with 3 additional related titles
class RelatedBooks extends Book {
    constructor() {
        super(...arguments);
        this.relatedTitle1 = arguments[8];
        this.relatedTitle2 = arguments[9];
        this.relatedTitle3 = arguments[10];
    }
}
//Creating the instance Origin's related books
const relatedBooksOrigin = new RelatedBooks(bookOrigin.title, bookOrigin.author, bookOrigin.genre, bookOrigin.publisher, 
                                            bookOrigin.pages, bookOrigin.ISBN, bookOrigin.edition, bookOrigin.language,
                                            "The Lost Order by Steve Berry", "The Da Vinci Code by Dan Brown", "Seven Deadly Wonders by Matthew Reilly");

//Declaration of the class author
class Author {
    constructor(name, bornInYear, genres, language) {
        this.name = name;
        this.bornInYear = bornInYear;
        this.genres = genres;
        this.language = language;
    }
}
//Creating the instance of our author
const authorOrigin = new Author("Dan Brown", "1964", "Thriller, adventure, mystery, conspiracy", "English");

//Declaration of the class publisher
class Publisher {
    constructor(name, establishedInYear, country, founder) {
        this.name = name;
        this.establishedInYear = establishedInYear;
        this.country = country;
        this.founder = founder;
    }
}
//Creating the instance of our publisher
const publisherOrigin= new Publisher("Doubleday", "1897", "United States of America", "Frank Nelson Doubleday and Samuel McClure");

//Function to generate content of the header
function header(headerText) {
    var list = document.getElementsByTagName("h1")[0];
    var text = document.createTextNode(headerText);
    list.appendChild(text);
  }
header("Info page");

//Function to generate content of the nav
function navBar(){
    var nav = document.getElementsByTagName("nav")[0];

    //Function to link the a.href to the various pages
    function navText(aString){
        var a = document.createElement("a");
        var aText = document.createTextNode(aString);
        switch (aString) {
            case "HOME":
                a.href = "index.html";
                break;
            case "INFO":
                a.href = "#";
                break;
            case "THE BOOK":
                a.href = "theBook.html";
                break;
            case "THE AUTHOR":
                a.href = "theAuthor.html";
                break;
            case "ORDER":
                a.href = "order.html";
                break;
            case "ABOUT US":
                a.href = "aboutUs.html";
                break;
            default:
                return;
        }
        a.appendChild(aText);
        nav.appendChild(a);
    }       

    var navTitles = ["HOME", "INFO", "THE BOOK", "THE AUTHOR", "ORDER", "ABOUT US"];
    for (var item of navTitles) {
    navText(item);
    }
}
navBar();

//Function to create an article with a header and paragraphs inside
function articleArea(elementId, artTitles, headerTitle, moreInfo, providedLink){
    var article = document.createElement("article");
    article.setAttribute("id", elementId);
    article.setAttribute("class", "lonelyArticle");
    document.body.appendChild(article);

    var articleHeader = document.createElement("h1");
    var articleHeaderText = document.createTextNode(headerTitle);
    articleHeader.setAttribute("class", "koptext");
    articleHeader.appendChild(articleHeaderText);
    article.appendChild(articleHeader);
    
    function artText(pString){
    var p = document.createElement("p");
    var pText = document.createTextNode(pString);
    p.appendChild(pText);
    article.appendChild(p);
    }       
    for (var item of artTitles) {
        artText(item);
    }

    var a = document.createElement("a");
    var aText = document.createTextNode(moreInfo);
    a.setAttribute("class", "moreInfoLink");
    a.setAttribute("href", providedLink);
    a.appendChild(aText);
    article.appendChild(a);
}

//Generating the first article
articleArea("bookArticle", ["Title: " + bookOrigin.title,
            "Author: " + bookOrigin.author,
            "Genre: " + bookOrigin.genre,
            "Publisher: " + bookOrigin.publisher,
            "Number of pages: " + bookOrigin.pages,
            "ISBN: " + bookOrigin.ISBN,
            "Edition: " + bookOrigin.edition,
            "Language: " + bookOrigin.language], "Information about the book",
            "Click here for more information about the book",
            "https://en.wikipedia.org/wiki/Origin_(Brown_novel)");

//Generating the second article
articleArea("authorArticle", ["Name: " + authorOrigin.name,
            "Born in year: " + authorOrigin.bornInYear,
            "Genres: " + authorOrigin.genres,
            "Language: " + authorOrigin.language], "Information about the author",
            "Click here for more information about the author",
            "https://en.wikipedia.org/wiki/Dan_Brown");

//Generating the third article
articleArea("publisherArticle", ["Name: " + publisherOrigin.name,
            "Established in year: " + publisherOrigin.establishedInYear,
            "Country: " + publisherOrigin.country,
            "Founder: " + publisherOrigin.founder], "Information about the publisher",
            "Click here for more information about the publisher",
            "https://en.wikipedia.org/wiki/Doubleday_(publisher)");

//Generating the fourth article
articleArea("relatedArticle", [relatedBooksOrigin.relatedTitle1,
            relatedBooksOrigin.relatedTitle2, 
            relatedBooksOrigin.relatedTitle3], "Related Books",
            "Click here for more information about books you should read after reading " + bookOrigin.title + " by " + authorOrigin.name,
            "https://www.quora.com/What-are-the-best-books-to-read-that-are-similar-to-Dan-Brown-books");
